package br.com.mastertech.imersivo.acessoconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcessoConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcessoConsumerApplication.class, args);
	}

}
