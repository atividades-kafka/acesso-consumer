package br.com.mastertech.imersivo.acessoconsumer.service;

import br.com.mastertech.imersivo.acessoproducer.model.Acesso;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class AcessoConsumerService {

    public void escreveArquivoAcessos(String arquivo, Acesso acesso) {
        try {
            String registro = String.format("%s,%s,%s\n", acesso.getCliente(), acesso.getPorta(), acesso.getTimestamp());
            Files.write(Paths.get(arquivo), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
