package br.com.mastertech.imersivo.acessoconsumer.controller;

import br.com.mastertech.imersivo.acessoproducer.model.Acesso;
import br.com.mastertech.imersivo.acessoconsumer.service.AcessoConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumerController {

    @Autowired
    private AcessoConsumerService service;

    @KafkaListener(topics = "acesso", groupId = "cugli")
    public void recebe(@Payload Acesso acesso){
        service.escreveArquivoAcessos("/dev/workspace/mastertech/acessos.csv", acesso);
    }
}
